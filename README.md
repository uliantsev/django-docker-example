## Run
```  
docker-compose up --build -d  
```  
or  
```  
docker-compose -f docker-compose-2.yml up --build -d
```  

## Check

```  
docker-compose ps
docker-compose logs
```  

