FROM python:3.9
COPY ./app /app
COPY ./run.sh /run.sh
ENV PYTHONPATH=/app
RUN pip install -r /app/requirements.txt
RUN chmod +x /run.sh
# CMD gunicorn app.stocks_products.wsgi -b 0.0.0.0:8000
CMD ["/run.sh"]
